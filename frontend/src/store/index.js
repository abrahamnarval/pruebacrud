import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tareas: [],
    editUser: null,
  },
  mutations: {
    setTareas(state, tareas){
      state.tareas = tareas;
    },

    setEditUser(state, tareas){
      state.editUser = tareas;
    }
  },
  actions: {
  },
  modules: {
  }
})
