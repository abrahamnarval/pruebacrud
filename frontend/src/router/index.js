import Vue from 'vue'
import VueRouter from 'vue-router'
import Tareas from '../views/Tareas.vue'
import CreateTareas from '../views/CreateTareas.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'tareas',
    component: Tareas
  },
  {
    path: '/createTareas',
    name: 'CreateTareas',
    component: CreateTareas
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
