'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tareas = sequelize.define('Tareas', {
    fecha: DataTypes.STRING,
    titulo: DataTypes.STRING,
    informacion: DataTypes.STRING,
  }, {});
  Tareas.associate = function(models) {
    
  };
  return Tareas;
};