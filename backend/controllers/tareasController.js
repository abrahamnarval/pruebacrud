
const { Tareas } = require('../models');
//ORM
async function getUnaTarea(req, res){
    let id = req.query.id;
    try {
        const tareas = await Tareas.findByPk(id);
        return res.status(200).send(tareas);
    } catch (error) {
        console.log(error);
        return res.status(500).send(`Internal server error: ${error}`);
    }
}

const getTareas = async(req, res)=>{
    try {
        let tareas = await Tareas.findAll();
        return res.status(200).send(tareas);
    } catch (error) {
        console.log(error);
        return res.status(500).send(`Internal server error: ${error}`);
    }
}

const createTareas = async(req, res)=>{
    try {
        await Tareas.create(req.body);
        return res.status(200).send('La tarea ha sido creada exitosamente');
    } catch (error) {
        console.log(error);
        return res.status(500).send(`Internal server error: ${error}`);
    }
}

const updateTareas = async(req, res)=>{
    console.log(req.body);
    const { id } = req.body;
    try {
        const tareas = await Tareas.findByPk(id);
        if(!tareas) return res.status(404).send('No existe una tarea con este id');
        await tareas.update(req.body)
        return res.status(200).send({message: `La tarea ha sido actualizada exitosamente`})
    } catch (error) {
        console.log(error);
        return res.status(500).send(`Internal server error: ${error}`);
    }
}


const deleteTareas = async(req, res)=>{
    let id = req.params.id;
    try {
        Tareas.destroy({
            where: {
                id
            }
        })
        return res.status(200).send('La tarea ha sido eliminada exitosamente');
    } catch (error) {
        return res.status(500).send(`Internal server error: ${error}`);
    }
}

module.exports = {
    getUnaTarea,
    getTareas,
    createTareas,
    updateTareas,
    deleteTareas
}

