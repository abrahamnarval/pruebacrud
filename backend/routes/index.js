
const express = require('express');
const api = express.Router()

const { getUnaTarea, getTareas ,createTareas, updateTareas, deleteTareas } = require('../controllers/tareasController');



/** Ruta News */
api.get('/getUnaTarea', getUnaTarea);
api.get('/getTareas', getTareas);
api.post('/createTareas', createTareas);
api.put('/updateTareas', updateTareas);
api.delete('/deleteTareas/:id', deleteTareas);



module.exports = api;
